let burgerContent = document.querySelector(".burger-content");
let mainContent = document.querySelector(".main-content")

document.querySelector(".burger").addEventListener("click", () => {
  burgerContent.classList.add("menu-opened");
  mainContent.classList.add("main-content-hide");
});

document.querySelector(".burger-close").addEventListener("click", () => {
  burgerContent.classList.remove("menu-opened");
  mainContent.classList.remove("main-content-hide");
});